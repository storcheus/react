import React, { Component } from 'react'
import {getCategories} from './UserFunctions'
import Table from 'react-bootstrap/Table'

class Category extends Component {
    constructor() {
        super()
        this.state = {
            categories: [],
        }
    }

    componentWillMount() {
        getCategories().then(response => {
            this.setState({
                categories: response,
            })
        })
    }

    render() {
        return (
            <div>
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th> id</th>
                        <th> title</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.categories.map(
                            (category) => {
                                return (
                                    <tr>
                                        <td> {category.id} </td>
                                        <td> {category.title} </td>
                                    </tr>
                                )
                            }
                        )
                    }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Category