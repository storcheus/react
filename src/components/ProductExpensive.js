import React, { Component } from 'react'
import {getProductExpensive} from './UserFunctions'
import Table from 'react-bootstrap/Table'

class ProductExpensive extends Component {
    constructor() {
        super()
        this.state = {
            records: []
        }
    }

    componentWillMount() {
        getProductExpensive().then(response => {
            this.setState({
                records: response,
            })
        })
    }

    render() {
        return (
            <div>
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>product pair</th>
                        <th>cost</th>
                    </tr>
                    </thead>
                    <tbody>

                    {
                        this.state.records.map(
                            (record) =>  {
                                return (
                                    <tr>
                                        <td>{record.title1} + {record.title2}</td>
                                        <td>= {record.total}</td>
                                    </tr>
                                )
                            }
                        )
                    }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default ProductExpensive
