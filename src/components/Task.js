import React, { Component } from 'react'
import ProductRelations from './ProductRelations'
import ProductCategories from './ProductCategories'
import ProductExpensive from './ProductExpensive'

class Task extends Component {

    render() {
        return (

            <div className="container">
                <div className="row justify-content-center">

                    <div className="col-md-4">
                        <div className="card-header">Block0 (relations)</div>
                            <ProductRelations/>
                    </div>

                    <div className="col-md-5">

                        <div className="card-header">Block1</div>
                            <ProductCategories/>
                    </div>

                    <div className="col-md-3">
                        <div className="card-header">Block2</div>
                            <ProductExpensive/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Task
