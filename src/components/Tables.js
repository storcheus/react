import React, { Component } from 'react'
import ProductRelations from './ProductRelations'
import Product from './Product'
import Category from './Category'

class Tables extends Component {

    render() {
        return (

            <div className="container">
                <div className="row justify-content-center">

                    <div className="col-md-4">

                        <div className="card-header">Product</div>
                            <Product/>
                    </div>

                    <div className="col-md-5">
                        <div className="card-header">Product & Category(relations)</div>
                            <ProductRelations/>
                   </div>
                    <div className="col-md-3">
                        <div className="card-header">Category</div>
                            <Category/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Tables
