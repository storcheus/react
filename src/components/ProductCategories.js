import React, { Component } from 'react'
import {getProductCategories} from './UserFunctions'
import Table from 'react-bootstrap/Table'

class ProductCategories extends Component {
    constructor() {
        super()
        this.state = {
            products: []
        }
    }

    componentWillMount() {
        getProductCategories().then(response => {
            this.setState({
                products: response,
            })
        })
    }

    render() {
        return (
            <div>
                    <Table striped bordered hover size="sm">
                        <thead>
                        <tr>
                            <th> id</th>
                            <th> title</th>
                            <th> count categories</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.products.map(
                                (product) => {
                                    return (
                                        <tr>
                                            <td> {product.id} </td>
                                            <td> {product.title} </td>
                                            <td> {product.count_categories} ({product.categories})</td>
                                        </tr>
                                    )
                                }
                            )
                        }
                        </tbody>
                    </Table>
            </div>
        )
    }
}

export default ProductCategories