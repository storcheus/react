import axios from 'axios-https-proxy-fix';

let host =  'http://localhost:8000';

export const register = newUser => {

return axios
        .post(host + '/api/register', newUser,
        {
            headers: { 'Content-Type': 'application/json' }
        })
        .then(response => {
            console.log(response)
        })
        .catch(err => {
            console.log(err)
        })
}

export const getProducts = () => {
    return axios
        .get(host + '/api/products', {
            headers: {Authorization: `Bearer ${localStorage.usertoken}`}
        })
        .then(response => {
            console.log(response)
            return response.data
        })
        .catch(err => {
            console.log(err)
        })
}

export const getCategories = () => {
    return axios
        .get(host + '/api/categories', {
            headers: {Authorization: `Bearer ${localStorage.usertoken}`}
        })
        .then(response => {
            console.log(response)
            return response.data
        })
        .catch(err => {
            console.log(err)
        })
}

export const login = user => {
    return axios
        .post(host+'/api/login',
            {
                email: user.email,
                password: user.password
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then(response => {
            localStorage.setItem('usertoken', response.data.token)
            return response.data.token
        })
        .catch(err => {
            console.log(err)
        })
}

export const getProductRelations = () => {
    return axios
        .get(host+'/api/product/relations',{
            headers: { Authorization: `Bearer ${localStorage.usertoken}` 
}
        })
        .then(response => {
            console.log(response)
            return response.data
        })
        .catch(err => {
            console.log(err)
        })
}

export const getProductCategories = () => {
    return axios
        .get(host+'/api/product/categories', {
            headers: { Authorization: `Bearer ${localStorage.usertoken}` }
        })
        .then(response => {
            console.log(response)
            return response.data
        })
        .catch(err => {
            console.log(err)
        })
}

export const getProductExpensive = () => {
    return axios
        .get(host+'/api/product/expensive', {
            headers: { Authorization: `Bearer ${localStorage.usertoken}` }
        })
        .then(response => {
            console.log(response)
            return response.data
        })
        .catch(err => {
            console.log(err)
        })
}
