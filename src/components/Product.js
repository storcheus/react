import React, { Component } from 'react'
import {getProducts} from './UserFunctions'
import Table from 'react-bootstrap/Table'

class Product extends Component {
    constructor() {
        super()
        this.state = {
            products: [],
        }
    }

    componentWillMount() {
        getProducts().then(response => {
            this.setState({
                products: response,
            })
        })
    }

    render() {
        return (
            <div>
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th> id</th>
                        <th> title</th>
                        <th> price</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.products.map(
                            (product) => {
                                return (
                                    <tr>
                                        <td> {product.id} </td>
                                        <td> {product.title} </td>
                                        <td> {product.title} </td>
                                    </tr>
                                )
                            }
                        )
                    }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default Product