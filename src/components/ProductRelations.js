import React, { Component } from 'react'
import {getProductRelations} from './UserFunctions'
import Table from 'react-bootstrap/Table'

class ProductRelations extends Component {
    constructor() {
        super()
        this.state = {
            relations: []
        }
    }

    componentWillMount() {
        getProductRelations().then(response => {
            this.setState({
                relations: response,
            })
        })
    }

    render() {
        return (
            <div>
                    <Table striped bordered hover size="sm">
                        <thead>
                        <tr>
                            <th> product id</th>
                            <th> product title</th>
                            <th> product price</th>
                            <th> category id</th>
                            <th> category title</th>
                        </tr>
                        </thead>
                        <tbody>

                        {
                            this.state.relations.map(
                                (relation, i) =>  {
                                    return (
                                        <tr>
                                            <td> {relation.product_id} </td>
                                            <td> {relation.product_title} </td>
                                            <td> {relation.product_price} </td>
                                            <td> {relation.category_id}  </td>
                                            <td> {relation.category_title} </td>
                                        </tr>
                                    )
                                }
                            )
                        }
                        </tbody>
                    </Table>
            </div>
        )
    }
}

export default ProductRelations
